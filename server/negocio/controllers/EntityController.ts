import { Request, Response } from 'express';
import RequestDto from '../../persistencia/dto/RequestDto';
import * as constantes from '../../negocio/constant/constantes';
import RolDao from '../../persistencia/dao/RolDao';
import RespuestaDto from '../../persistencia/dto/ResponseDto';
import DaoAbstract from '../../persistencia/generico/abstract/DaoAbstract';
import ConfigDao from '../../persistencia/dao/ConfigDao';
import DataDao from '../../persistencia/dao/DataDao';
import DataTypeDao from '../../persistencia/dao/DataTypeDao';
import EnvironmentDao from '../../persistencia/dao/EnvironmentDao';
import TypeConfigDao from '../../persistencia/dao/TypeConfigDao';
import UserDao from '../../persistencia/dao/UserDao';
import UserStateDao from '../../persistencia/dao/UserStateDao';
import ResponseDto from '../../persistencia/dto/ResponseDto';
import EntityAbstract from '../../persistencia/generico/abstract/EntityAbstract';

export default class EntityController {

    requestObj: RequestDto;
    responseObj: RespuestaDto;

    constructor() {
        this.requestObj = new RequestDto();
        this.responseObj = new RespuestaDto();
    }

    /**
     * Manejador de la petición GET
     */
    async index(req: Request, res: Response) {

        // var handlerRoles: RolDao = new RolDao();

        // var respuesta = await handlerRoles.getAllItems();
        // var rol = await handlerRoles.getItem(req.query.id);

        var handlerDao: DaoAbstract = this.getHandler(req.query.type);
        var respuesta = await handlerDao.getAllItems();

        res.send({
            query: req.query.id,
            type: req.query.type,
            respuesta: respuesta,
        });
    }

    /**
     * Función para obtener una entidad en particular
     */
    async getItem(req: Request, res: Response) {
        console.log(req.params);
        var handlerDao: DaoAbstract = this.getHandler(req.params.type);

        // Si el manejador existe
        if (handlerDao != undefined) {
            var entity = await handlerDao.getItem(req.params.id);
            if (entity instanceof EntityAbstract) {
                res.send({
                    respuesta: req.params,
                    entity: entity
                });
            }

            res.send({
                code: this.requestObj.getCode(),
                message: "No se encontraron entidades con el id recibido",
                data: ""
            });
        }

        res.status(403).send({
            code: this.requestObj.getCode(),
            message: "No se reconoce el código enviado",
            data: ""
        });
    }

    /**
     * Manejador de la petición POST
     */
    async post(req: Request, res: Response) {
        this.getRequestParameters(req);

        if (req.params.type == this.requestObj.getData().entity_type) {
            const entity_type = this.requestObj.getData().entity_type;
            var responseDto = new ResponseDto();
            var handlerDao: DaoAbstract = this.getHandler(entity_type);

            // Si el manejador existe
            if (handlerDao != undefined) {
                // Si el código de servicio y las entidades son verificadas
                if (this.requestObj.verifyCode(constantes.ENTITY_CREATE) &&
                    handlerDao.verifyJsonEntity(this.requestObj.getData().entities)) {

                    var entities = handlerDao.fromJson(this.requestObj.getData().entities);
                    entities = await handlerDao.saveEntities(entities, handlerDao.getSql);

                    responseDto.sendResponse(res, constantes.ENTITY_CREATE, 'Entidad creada con éxito', {
                        entity: handlerDao.toString(),
                        results: entities
                    });
                    return;
                }

                res.status(403).send({
                    code: this.requestObj.getCode(),
                    message: "No se reconoce el código enviado",
                    data: ""
                });
            }

            res.status(403).send({
                code: this.requestObj.getCode(),
                message: "No se reconoce el tipo de entidad enviado",
                data: ""
            });
        }

        res.status(403).send({
            code: this.requestObj.getCode(),
            message: "Revise por favor los datos enviados",
            data: ""
        });
    }

    /**
     * Manejador de la petición PATCH
     */
    patch(req: Request, res: Response) {
        this.getRequestParameters(req);

        if (req.params.type == this.requestObj.getData().entity_type) {

            const entity_type = this.requestObj.getData().entity_type;
            var responseDto = new ResponseDto();
            var handlerDao: DaoAbstract = this.getHandler(entity_type);

            // Si el manejador existe
            if (handlerDao != undefined) {
                if (this.requestObj.getCode() == constantes.ENTITY_UPDATE &&
                    handlerDao.verifyJsonEntity(this.requestObj.getData().entities)) {
                    res.status(200).send({
                        code: this.requestObj.getCode(),
                        message: "Se ha recibido la petición PATCH"
                    });
                    return;
                }
        
                res.status(403).send({
                    code: this.requestObj.getCode(),
                    message: "No se reconoce el código enviado"
                });
            }
        }

        res.status(403).send({
            code: this.requestObj.getCode(),
            message: "Revise por favor los datos enviados",
            data: ""
        });
    }

    /**
     * Manejador de la petición DELETE
     */
    delete(req: Request, res: Response) {
        this.getRequestParameters(req);

        if (this.requestObj.getCode() == constantes.ENTITY_DELETE) {
            res.status(200).send({
                code: this.requestObj.getCode(),
                message: "Se ha recibido la petición DELETE"
            });
            return;
        }

        res.status(403).send({
            code: this.requestObj.getCode(),
            message: "No se reconoce el código enviado"
        });
    }

    /**
     * Función para obtener los parámetros del request y se devuleve el objeto
     * de tipo dto
     * 
     * @param req objeto del request solicitado
     */
    getRequestParameters(req: Request): void {
        this.requestObj.setFields(req.body.code, req.body.message, req.body.data);
    }

    /**
     * Función que retorna el manejador según haya llegado en la
     * petición
     * 
     * @param entity nombre del tipo de entidad
     */
    getHandler(entity: string): any {
        switch (entity) {
            case 'rol':
                return new RolDao();
            case 'config':
                return new ConfigDao();
            case 'data':
                return new DataDao();
            case 'data_type':
                return new DataTypeDao();
            case 'environment':
                return new EnvironmentDao();
            case 'type_config':
                return new TypeConfigDao();
            case 'user':
                return new UserDao();
            case 'user_state':
                return new UserStateDao();
            default:
                return undefined;
        }
    }

}