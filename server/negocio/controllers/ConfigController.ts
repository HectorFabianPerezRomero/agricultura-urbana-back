import * as constantes from '../../negocio/constant/constantes';
import * as mysql from 'mysql2/promise';

export default class ConfigController {

    connection: any;

    constructor() {
        this.connectionDB();
    }

    async connectionDB(){
        this.connection = await mysql.createConnection({
            host: constantes.db_host,
            user: constantes.db_user_name,
            password: constantes.db_user_pass,
            database: constantes.db_name
        });
    }

    async checkConfig(){

        console.log("Iniciando servidor...");
        console.log("Revisando base de datos");

        let sql = "SELECT count(*) FROM ?? WHERE id_type_config = ? AND id = ?";

        await this.connectionDB();
        const [results] = await this.connection.query(sql, ['config','1','1']);

        console.log(results);
    }

}