'use strict';

export const db_host: string = 'localhost';
export const db_name: string = 'db_agro_urb';
export const db_user_name: string = 'root';
export const db_user_pass: string = '';


/*********************************/
/**** Constantes de servicios ****/
/*********************************/

/**
 * Constantes de entrada de servicios de Entities
 */
export const ENTITY_GET: number = 1;
export const ENTITY_CREATE: number = 2;
export const ENTITY_UPDATE: number = 3;
export const ENTITY_DELETE: number = 4;

/**
 * Constantes de salida de servicios de Entities
 */
