import * as express from "express";

// import sub-routers
import homeRoutes from "./HomeRoutes";
import entityRoutes from "./EntityRoutes";

let router = express.Router();

router.use('/', homeRoutes);
router.use('/entity', entityRoutes);

export = router;