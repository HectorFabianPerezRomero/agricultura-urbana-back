import * as express from "express";

import HomeController from '../controllers/HomeController';
    
let router = express.Router();
let homeController = new HomeController();

router.get('/', homeController.index.bind(homeController));

export = router;