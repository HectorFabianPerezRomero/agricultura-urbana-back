import * as express from "express";

import EntityController from '../controllers/EntityController';
    
let router = express.Router();
let entityController = new EntityController();

router.get('/', entityController.index.bind(entityController));
router.get('/:type/:id', entityController.getItem.bind(entityController));

router.post('/:type', entityController.post.bind(entityController));
router.patch('/:type', entityController.patch.bind(entityController));
router.delete('/:type', entityController.delete.bind(entityController));
export = router;