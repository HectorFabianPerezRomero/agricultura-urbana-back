CREATE TABLE db_agro_urb.user_state (
	id	integer PRIMARY KEY AUTO_INCREMENT,
    name_item VARCHAR(30) NOT NULL
);

CREATE TABLE db_agro_urb.roles (
	id integer PRIMARY KEY AUTO_INCREMENT,
    name_item VARCHAR(30) NOT NULL
);

CREATE TABLE db_agro_urb.user (
	id integer PRIMARY KEY AUTO_INCREMENT,
    id_user_state integer NOT NULL,
    id_roles integer NOT NULL,
    name_item VARCHAR(30) NOT NULL,
    email varchar(100) NOT NULL,
    first_name varchar(50) NOT NULL,
    last_name varchar(50) NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_user_state) REFERENCES db_agro_urb.user_state(id),
    FOREIGN KEY (id_roles) REFERENCES db_agro_urb.roles (id)
);

CREATE TABLE db_agro_urb.type_config(
    id integer PRIMARY KEY AUTO_INCREMENT,
    name_item VARCHAR(30) NOT NULL
);

CREATE TABLE db_agro_urb.config(
    id integer PRIMARY KEY AUTO_INCREMENT,
    id_user integer NOT NULL,
    name_item VARCHAR(30) NOT NULL,
    id_type_config integer NOT NULL,
    config text NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_type_config) REFERENCES db_agro_urb.type_config(id),
    FOREIGN KEY (id_user) REFERENCES db_agro_urb.user(id)
);

CREATE TABLE db_agro_urb.environment(
    id integer PRIMARY KEY AUTO_INCREMENT,
    id_user integer NOT NULL,
    id_config integer NOT NULL,
    name_item VARCHAR(30) NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_user) REFERENCES db_agro_urb.user(id),
    FOREIGN KEY (id_config) REFERENCES db_agro_urb.config(id)
);

CREATE TABLE db_agro_urb.data_type(
	id integer PRIMARY KEY AUTO_INCREMENT,
    name_item VARCHAR(30) NOT NULL
);

CREATE TABLE db_agro_urb.data(
	id integer PRIMARY KEY AUTO_INCREMENT,
    name_item varchar(100) NOT NULL,
    id_data_type integer NOT NULL,
    id_environment integer NOT NULL,
    data text,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_data_type) REFERENCES db_agro_urb.data_type(id),
    FOREIGN KEY (id_environment) REFERENCES db_agro_urb.environment(id)
);

CREATE TABLE db_agro_urb.entity_type(
	id integer PRIMARY KEY AUTO_INCREMENT,
    name_item VARCHAR(30) NOT NULL
);

CREATE TABLE db_agro_urb.entity(
    id integer PRIMARY KEY AUTO_INCREMENT,
    id_user integer NOT NULL,
    id_entity_type integer NOT NULL,
    id_target integer NOT NULL,
    name_item VARCHAR(30) NOT NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_entity_type) REFERENCES db_agro_urb.entity_type(id),
    FOREIGN KEY (id_user) REFERENCES db_agro_urb.user(id)
);

CREATE TABLE db_agro_urb.type_log(
    id integer PRIMARY KEY AUTO_INCREMENT,
    name_item VARCHAR(30) NOT NULL
);

CREATE TABLE db_agro_urb.log(
    id integer PRIMARY KEY AUTO_INCREMENT,
    name_item VARCHAR(30) NOT NULL,
    id_user integer DEFAULT 1,
    id_type_log integer DEFAULT 1,
    description text,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (id_user) REFERENCES db_agro_urb.user(id),
    FOREIGN KEY (id_type_log) REFERENCES db_agro_urb.type_log(id)
);

/* Config types */
INSERT INTO db_agro_urb.type_config (name_item) VALUES('server');
INSERT INTO db_agro_urb.type_config (name_item) VALUES('user');
INSERT INTO db_agro_urb.type_config (name_item) VALUES('entity');
INSERT INTO db_agro_urb.type_config (name_item) VALUES('services');
INSERT INTO db_agro_urb.type_config (name_item) VALUES('environment');

/* User states */
INSERT INTO db_agro_urb.user_state (name_item) VALUES('block');
INSERT INTO db_agro_urb.user_state (name_item) VALUES('active');

/* Data types */
INSERT INTO db_agro_urb.data_type (name_item) VALUES('Temperatura');
INSERT INTO db_agro_urb.data_type (name_item) VALUES('Luz');
INSERT INTO db_agro_urb.data_type (name_item) VALUES('Humedad');
INSERT INTO db_agro_urb.data_type (name_item) VALUES('ph');
INSERT INTO db_agro_urb.data_type (name_item) VALUES('Temperatura');

/* Enity types */
INSERT INTO db_agro_urb.entity_type (name_item) VALUES('config');
INSERT INTO db_agro_urb.entity_type (name_item) VALUES('data');
INSERT INTO db_agro_urb.entity_type (name_item) VALUES('data_type');
INSERT INTO db_agro_urb.entity_type (name_item) VALUES('data_config');
INSERT INTO db_agro_urb.entity_type (name_item) VALUES('environment');
INSERT INTO db_agro_urb.entity_type (name_item) VALUES('user');
INSERT INTO db_agro_urb.entity_type (name_item) VALUES('user_state');

/* User roles */
INSERT INTO db_agro_urb.roles (name_item) VALUES('admin');
INSERT INTO db_agro_urb.roles (name_item) VALUES('user');
INSERT INTO db_agro_urb.roles (name_item) VALUES('anonymous');

SELECT @active_id := id FROM db_agro_urb.user_state WHERE name_item = 'active';
SELECT @admin_role := id FROM db_agro_urb.roles WHERE name_item = 'admin';
/* User Admin */
INSERT INTO db_agro_urb.user (id_user_state,id_roles,name_item,first_name,last_name) 
    VALUES(@active_id,@admin_role,'admin','admin','admin');

INSERT INTO db_agro_urb.type_log (name_item) VALUES ('status');
INSERT INTO db_agro_urb.type_log (name_item) VALUES ('warning');
INSERT INTO db_agro_urb.type_log (name_item) VALUES ('error');
INSERT INTO db_agro_urb.type_log (name_item) VALUES ('update');

SELECT @admin_id := id FROM db_agro_urb.user WHERE first_name = 'admin';
SELECT @type_log_id := id FROM db_agro_urb.type_log WHERE name_item = 'status';
INSERT INTO db_agro_urb.log (name_item,id_user,id_type_log,description) 
VALUES ('Base de datos',@admin_id,@type_log_id, "Se ha creado la base de datos del sitio");