import * as express from "express";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class Log extends EntityAbstract{

    private id_user: number;
    private id_type_log: number;
    private description: string;

    constructor(){
        super();
        this.id_user = 0;
        this.id_type_log = 0;
        this.description = '';
    }

    /**
     * Getter $id_user
     * @return {number}
     */
	public get $id_user(): number {
		return this.id_user;
	}

    /**
     * Setter $id_user
     * @param {number} value
     */
	public set $id_user(value: number) {
		this.id_user = value;
	}

    /**
     * Getter $id_type_log
     * @return {number}
     */
	public get $id_type_log(): number {
		return this.id_type_log;
	}

    /**
     * Setter $id_type_log
     * @param {number} value
     */
	public set $id_type_log(value: number) {
		this.id_type_log = value;
	}

    /**
     * Getter $description
     * @return {string}
     */
	public get $description(): string {
		return this.description;
	}

    /**
     * Setter $description
     * @param {string} value
     */
	public set $description(value: string) {
		this.description = value;
	}

}