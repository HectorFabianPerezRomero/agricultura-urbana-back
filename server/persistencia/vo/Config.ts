import * as express from "express";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class Config extends EntityAbstract{

    private id_user: number;
    private id_type_config: number;
    private config: string;

	constructor() {
        super();
		this.id_user = 1;
		this.id_type_config = 1;
		this.config = '';
    }    

    /**
     * Getter $id_user
     * @return {number}
     */
	public get $id_user(): number {
		return this.id_user;
	}

    /**
     * Setter $id_user
     * @param {number} value
     */
	public set $id_user(value: number) {
		this.id_user = value;
	}

    /**
     * Getter $id_type_config
     * @return {number}
     */
	public get $id_type_config(): number {
		return this.id_type_config;
	}

    /**
     * Setter $id_type_config
     * @param {number} value
     */
	public set $id_type_config(value: number) {
		this.id_type_config = value;
	}

    /**
     * Getter $config
     * @return {string}
     */
	public get $config(): string {
		return this.config;
	}

    /**
     * Setter $config
     * @param {string} value
     */
	public set $config(value: string) {
		this.config = value;
	}

}