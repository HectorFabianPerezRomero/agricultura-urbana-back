import * as express from "express";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class Entity extends EntityAbstract{

    private id_user: number;
    private id_entity_type: number;
    private id_target: number;

    constructor(){
        super();
        this.id_user = 0;
        this.id_entity_type = 0;
        this.id_target = 0;
    }

    /**
     * Getter $id_user
     * @return {number}
     */
	public get $id_user(): number {
		return this.id_user;
	}

    /**
     * Setter $id_user
     * @param {number} value
     */
	public set $id_user(value: number) {
		this.id_user = value;
	}

    /**
     * Getter $id_entity_type
     * @return {number}
     */
	public get $id_entity_type(): number {
		return this.id_entity_type;
	}

    /**
     * Setter $id_entity_type
     * @param {number} value
     */
	public set $id_entity_type(value: number) {
		this.id_entity_type = value;
	}

    /**
     * Getter $id_target
     * @return {number}
     */
	public get $id_target(): number {
		return this.id_target;
	}

    /**
     * Setter $id_target
     * @param {number} value
     */
	public set $id_target(value: number) {
		this.id_target = value;
	}

}