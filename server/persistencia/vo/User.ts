import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class User extends EntityAbstract{

    private id_user_state: number;
    private id_roles: number;
    private email: string;
    private first_name: string;
    private last_name: string;

    constructor() {
        super();
        this.id_user_state = 1;
        this.id_roles = 2;
        this.email = '';
        this.first_name = '';
        this.last_name = '';
    }

    /**
     * Getter $id_user_state
     * @return {number}
     */
	public get $id_user_state(): number {
		return this.id_user_state;
	}

    /**
     * Setter $id_user_state
     * @param {number} value
     */
	public set $id_user_state(value: number) {
		this.id_user_state = value;
	}

    /**
     * Getter $id_roles
     * @return {number}
     */
	public get $id_roles(): number {
		return this.id_roles;
	}

    /**
     * Setter $id_roles
     * @param {number} value
     */
	public set $id_roles(value: number) {
		this.id_roles = value;
	}


    /**
     * Getter $email
     * @return {string}
     */
	public get $email(): string {
		return this.email;
	}

    /**
     * Setter $email
     * @param {string} value
     */
	public set $email(value: string) {
		this.email = value;
	}

    /**
     * Getter $first_name
     * @return {string}
     */
	public get $first_name(): string {
		return this.first_name;
	}

    /**
     * Setter $first_name
     * @param {string} value
     */
	public set $first_name(value: string) {
		this.first_name = value;
	}

    /**
     * Getter $last_name
     * @return {string}
     */
	public get $last_name(): string {
		return this.last_name;
	}

    /**
     * Setter $last_name
     * @param {string} value
     */
	public set $last_name(value: string) {
		this.last_name = value;
	}
        
}