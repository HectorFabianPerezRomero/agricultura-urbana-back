import * as express from "express";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class Data extends EntityAbstract{

    private id_data_type: number;
    private id_environment: number;
    private data: string;

    constructor(){
        super();
        this.id_data_type = 0;
        this.id_environment = 0;
        this.data = '';
    }

    /**
     * Getter $id_data_type
     * @return {number}
     */
	public get $id_data_type(): number {
		return this.id_data_type;
	}

    /**
     * Setter $id_data_type
     * @param {number} value
     */
	public set $id_data_type(value: number) {
		this.id_data_type = value;
	}

    /**
     * Getter $id_environment
     * @return {number}
     */
	public get $id_environment(): number {
		return this.id_environment;
	}

    /**
     * Setter $id_environment
     * @param {number} value
     */
	public set $id_environment(value: number) {
		this.id_environment = value;
	}

    /**
     * Getter $data
     * @return {string}
     */
	public get $data(): string {
		return this.data;
	}

    /**
     * Setter $data
     * @param {string} value
     */
	public set $data(value: string) {
		this.data = value;
	}

}