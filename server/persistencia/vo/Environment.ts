import * as express from "express";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class Environment extends EntityAbstract{

    private id_user: number;
    private id_config: number;

    constructor(){
        super();
        this.id_user = 0;
        this.id_config = 0;
    }

    /**
     * Getter $id_user
     * @return {number}
     */
	public get $id_user(): number {
		return this.id_user;
	}

    /**
     * Setter $id_user
     * @param {number} value
     */
	public set $id_user(value: number) {
		this.id_user = value;
	}

    /**
     * Getter $id_config
     * @return {number}
     */
	public get $id_config(): number {
		return this.id_config;
	}

    /**
     * Setter $id_config
     * @param {number} value
     */
	public set $id_config(value: number) {
		this.id_config = value;
	}

}