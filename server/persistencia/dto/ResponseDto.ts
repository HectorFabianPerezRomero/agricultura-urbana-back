import DtoAbstract from "../generico/abstract/DtoAbstract";
import { Response } from "express";

export default class ResponseDto extends DtoAbstract{

    constructor(){
        super();
    }

    /**
     * Función para devolver un error en la petición
     * @param response Objeto que maneja la respuesta del servicio
     * @param errorCode Código de error a devolver en el servicio
     * @param internalCode Código de error interno a devolver en el servicio
     * @param message Mensaje de error a devolver en el servicio
     */
    sendError(res: Response,errorCode: number, internalCode: number ,message: string){

        res.status(errorCode).send({
            code: internalCode,
            error: message,
            data: this.data
        });
    }

    /**
     * Función para devolver un error en la petición
     * @param response Objeto que maneja la respuesta del servicio
     */
    sendResponse(res: Response, code: number, message: string, data: any){

        res.send({
            code: code,
            message: message,
            data: data
        });
    }
}