import DtoAbstract from "../generico/abstract/DtoAbstract";

export default class RequestDto extends DtoAbstract{

    constructor(){
        super();
    }

    verifyCode(code: number){
        if(this.code == code)
            return true;
        
        return false;
    }

}