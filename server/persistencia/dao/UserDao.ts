import DaoAbstract from "../generico/abstract/DaoAbstract";
import User from "../vo/User";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class UserDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var user = new User();
        user.setId(datos.id);
        user.setName(datos.name_item);
        user.$email = datos.email;
        user.$first_name = datos.first_name;
        user.$last_name = datos.last_name;
        user.$id_roles = datos.id_roles;
        user.$id_user_state = datos.id_user_state;
        return user;
    };

    fromJson(jsonData: any): User[]{
        var users: User[] = [];

        for(let user of jsonData){
            var userTemp = new User();
            userTemp.setName(user.name_item);
            user.$email = user.email;
            user.$first_name = user.first_name;
            user.$last_name = user.last_name;
            user.$id_roles = user.id_roles;
            user.$id_user_state = user.id_user_state;
            users.push(userTemp);
        }
        return users;
    }

    getSql(entity: User): string{
        return 'INSERT INTO data (name_item,id_user_state,id_roles,email,first_name,last_name) ' +
        'VALUES ("' + entity.getName() + '",' +
        entity.$id_user_state + ',' +
        entity.$id_roles + ', "' +
        entity.$email + ', "' +
        entity.$first_name + ', "' +
        entity.$last_name +
        '");';
    }

    async getAllItems(): Promise<User[]>{
        return await this.getAll("user",this.getObject);
    }

    async getItem(id: string): Promise<User>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['user',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData)
            validate = validate && entity.hasOwnProperty("name_item") &&
            entity.hasOwnProperty("id_user_state") &&
            entity.hasOwnProperty("id_roles") &&
            entity.hasOwnProperty("email") &&
            entity.hasOwnProperty("first_name") &&
            entity.hasOwnProperty("last_name");
            
        return validate;
    }
    
    toString(){
        return "User Dao";
    }
}