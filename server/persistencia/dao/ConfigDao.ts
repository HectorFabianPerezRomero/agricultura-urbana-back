import DaoAbstract from "../generico/abstract/DaoAbstract";
import Config from "../vo/Config";
import EntityAbstract from "../generico/abstract/EntityAbstract";
import Entity from "../vo/Entity";

export default class ConfigDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var config = new Config();
        config.setId(datos.id);
        config.setName(datos.name_item);
        config.$id_user = datos.id_user;
        config.$id_type_config = datos.id_type_config;
        config.$config = datos.config;
        return config;
    };

    fromJson(jsonData: any): any[]{
        var configs: Config[] = [];

        for(let config of jsonData){
            var configTemp = new Config();
            configTemp.setName(config.name_item);
            configTemp.$config = config.config;
            configTemp.$id_type_config = config.id_type_config;
            configTemp.$id_user = config.id_user;
            configs.push(configTemp);
        }
        return configs;
    }

    getSql(entity: Config): string{
        return 'INSERT INTO config (name_item,id_type_config,id_user,config) ' +
        'VALUES ("' + entity.getName() + '",' +
        entity.$id_type_config + ',' +
        entity.$id_user + ', "' +
        entity.$config +
        '");';
    }

    async getAllItems(): Promise<Config[]>{
        return await this.getAll("config",this.getObject);
    }

    async getItem(id: string): Promise<Config>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['config',id] , this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData){
            validate = validate && entity.hasOwnProperty("name_item") &&
            entity.hasOwnProperty("id_user") &&
            entity.hasOwnProperty("id_type_config") &&
            entity.hasOwnProperty("config");
        }
        return validate;
    }
    
    toString(){
        return "Config Dao";
    }
}