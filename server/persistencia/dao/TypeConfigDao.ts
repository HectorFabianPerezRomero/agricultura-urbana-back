import DaoAbstract from "../generico/abstract/DaoAbstract";
import TypeConfig from "../vo/TypeConfig";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class TypeConfigDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var typeConfig = new TypeConfig();
        typeConfig.setId(datos.id);
        typeConfig.setName(datos.name_item);
        return typeConfig;
    };

    fromJson(jsonData: any): TypeConfig[]{
        var typeConfigs: TypeConfig[] = [];

        for(let typeConfig of jsonData){
            var typeConfigTemp = new TypeConfig();
            typeConfigTemp.setName(typeConfig.name_item);
            typeConfigs.push(typeConfigTemp);
        }
        return typeConfigs;
    }

    getSql(entity: EntityAbstract): string{
        return 'INSERT INTO roles (name_item) VALUES ("' + entity.getName() + '");';
    }

    async getAllItems(): Promise<TypeConfig[]>{
        return await this.getAll("type_config",this.getObject);
    }

    async getItem(id: string): Promise<TypeConfig>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['type_config',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData)
            validate = validate && entity.hasOwnProperty("name_item");
            
        return validate;
    }
    
    toString(){
        return "Type config Dao";
    }
}