import DaoAbstract from "../generico/abstract/DaoAbstract";
import Data from "../vo/Data";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class DataDao extends DaoAbstract{
    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var data = new Data();
        data.setId(datos.id);
        data.setName(datos.name_item);
        data.$data = datos.data;
        data.$id_data_type = datos.id_data_type;
        data.$id_environment = datos.id_environment;
        return data;
    };

    fromJson(jsonData: any): Data[]{
        var datas: Data[] = [];

        for(let data of jsonData){
            var dataTemp = new Data();
            dataTemp.setName(data.name_item);
            dataTemp.$data = data.data;
            dataTemp.$id_data_type = data.id_data_type;
            dataTemp.$id_environment = data.id_environment;
            datas.push(dataTemp);
        }
        return datas;
    }

    getSql(entity: Data): string{
        return 'INSERT INTO data (name_item,id_data_type,id_environment,data) ' +
        'VALUES ("' + entity.getName() + '",' +
        entity.$id_data_type + ',' +
        entity.$id_environment + ', "' +
        entity.$data +
        '");';
    }

    async getAllItems(): Promise<Data[]>{
        return await this.getAll("data", this.getObject);
    }

    async getItem(id: string): Promise<Data>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['data',id] , this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData){
            validate = validate && entity.hasOwnProperty("name_item") &&
            entity.hasOwnProperty("id_data_type") &&
            entity.hasOwnProperty("id_environment") &&
            entity.hasOwnProperty("data");
        }
        return validate;
    }
    
    toString(){
        return "Data Dao";
    }
}