import DaoAbstract from "../generico/abstract/DaoAbstract";
import DataType from "../vo/DataType";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class DataTypeDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var dataType = new DataType();
        dataType.setId(datos.id);
        dataType.setName(datos.name_item);
        return dataType;
    };

    fromJson(jsonData: any): DataType[]{
        var dataTypes: DataType[] = [];

        for(let dataType of jsonData){
            var dataTypeTemp = new DataType();
            dataTypeTemp.setName(dataType.name_item);
            dataTypes.push(dataTypeTemp);
        }
        return dataTypes;
    }

    getSql(entity: EntityAbstract): string{
        return 'INSERT INTO roles (name_item) VALUES ("' + entity.getName() + '");';
    }

    async getAllItems(): Promise<DataType[]>{
        return await this.getAll("data_type",this.getObject);
    }

    async getItem(id: string): Promise<DataType>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['data_type',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData)
            validate = validate && entity.hasOwnProperty("name_item");
            
        return validate;
    }
    
    toString(){
        return "Data Type Dao";
    }
}