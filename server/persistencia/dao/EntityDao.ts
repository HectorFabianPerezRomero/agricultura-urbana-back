import DaoAbstract from "../generico/abstract/DaoAbstract";
import Entity from "../vo/Entity";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class EntityDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var entity = new Entity();
        entity.setId(datos.id);
        entity.setName(datos.name_item);
        entity.$id_entity_type = datos.id_entity_type;
        entity.$id_target = datos.ir_target;
        entity.$id_user = datos.id_user;
        return entity;
    };

    fromJson(jsonData: any): Entity[]{
        var entities: Entity[] = [];

        for(let entity of jsonData){
            var entityTemp = new Entity();
            entityTemp.setName(entity.name_item);
            entityTemp.$id_entity_type = entity.id_entity_type;
            entityTemp.$id_target = entity.ir_target;
            entityTemp.$id_user = entity.id_user;
            entities.push(entityTemp);
        }
        return entities;
    }

    getSql(entity: Entity): string{
        return 'INSERT INTO data (name_item,id_user,id_entity_type,id_target) ' +
        'VALUES ("' + entity.getName() + '",' +
        entity.$id_user + ',' +
        entity.$id_entity_type + ', "' +
        entity.$id_target +
        '");';
    }

    async getAllItems(): Promise<Entity[]>{
        return await this.getAll("entity",this.getObject);
    }

    async getItem(id: string): Promise<Entity>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['entity',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData){
            validate = validate && entity.hasOwnProperty("name_item") &&
            entity.hasOwnProperty("id_user") &&
            entity.hasOwnProperty("id_entity_type") &&
            entity.hasOwnProperty("id_target");
        }
        return validate;
    }
    
    toString(){
        return "Entity Dao";
    }
}