import DaoAbstract from "../generico/abstract/DaoAbstract";
import UserState from "../vo/UserState";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class UserStateDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var userState = new UserState();
        userState.setId(datos.id);
        userState.setName(datos.name_item);
        return userState;
    };

    fromJson(jsonData: any): UserState[]{
        var userStates: UserState[] = [];

        for(let userState of jsonData){
            var userStateTemp = new UserState();
            userStateTemp.setName(userState.name_item);
            userStates.push(userStateTemp);
        }
        return userStates;
    }

    getSql(entity: EntityAbstract): string{
        return 'INSERT INTO roles (name_item) VALUES ("' + entity.getName() + '");';
    }

    async getAllItems(): Promise<UserState[]>{
        return await this.getAll("user_state",this.getObject);
    }

    async getItem(id: string): Promise<UserState>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['user_state',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData)
            validate = validate && entity.hasOwnProperty("name_item");
            
        return validate;
    }
    
    toString(){
        return "User state Dao";
    }
}