import DaoAbstract from "../generico/abstract/DaoAbstract";
import Roles from "../vo/Roles";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class RolDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var rol = new Roles();
        rol.setId(datos.id);
        rol.setName(datos.name_item);
        return rol;
    };

    fromJson(jsonData: any): Roles[]{
        var roles: Roles[] = [];

        for(let rol of jsonData){
            var rolTemp = new Roles();
            rolTemp.setName(rol.name_item);
            roles.push(rolTemp);
        }
        return roles;
    }

    getSql = function(entity: EntityAbstract): string{
        return 'INSERT INTO roles (name_item) VALUES ("' + entity.getName() + '");';
    }

    async getAllItems(): Promise<Roles[]>{
        return await this.getAll("roles",this.getObject);
    }

    async getItem(id: string): Promise<Roles>{
        // return await this.get("SELECT * FROM roles WHERE `id` = ?", [id] ,this.getObject);
        return await this.get("SELECT * FROM roles WHERE `id` = ?", [id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: any):boolean{
        var validate = true;
        for(let entity of jsonData)
            validate = validate && entity.hasOwnProperty("name_item");
            
        return validate;
    };
    
    toString(){
        return "Rol Dao";
    }
}