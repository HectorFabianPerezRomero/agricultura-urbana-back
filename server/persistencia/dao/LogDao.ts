import DaoAbstract from "../generico/abstract/DaoAbstract";
import Log from "../vo/Log";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class LogDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var log = new Log();
        log.setId(datos.id);
        log.setName(datos.name_item);
        log.$description = datos.description;
        log.$id_type_log = datos.id_type_log;
        log.$id_user = datos.id_user;
        return log;
    };

    fromJson(jsonData: any): Log[]{
        var logs: Log[] = [];

        for(let log of jsonData){
            var logTemp = new Log();
            logTemp.setName(log.name_item);
            log.$description = log.description;
            log.$id_type_log = log.id_type_log;
            log.$id_user = log.id_user;
            logs.push(logTemp);
        }
        return logs;
    }

    getSql(entity: Log): string{
        return 'INSERT INTO data (name_item,id_user,id_type_log,description) ' +
        'VALUES ("' + entity.getName() + '",' +
        entity.$id_user + ',' +
        entity.$id_type_log + ', "' +
        entity.$description +
        '");';
    }

    async getAllItems(): Promise<Log[]>{
        return await this.getAll("log",this.getObject);
    }

    async getItem(id: string): Promise<Log>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['log',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData){
            validate = validate && entity.hasOwnProperty("name_item") &&
            entity.hasOwnProperty("id_user") &&
            entity.hasOwnProperty("id_type_log") &&
            entity.hasOwnProperty("description");
        }
            
        return validate;
    }
    
    toString(){
        return "Rol Dao";
    }
}