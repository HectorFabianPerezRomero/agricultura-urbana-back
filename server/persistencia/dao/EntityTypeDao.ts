import DaoAbstract from "../generico/abstract/DaoAbstract";
import EntityType from "../vo/EntityType";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class EntityTypeDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var entityType = new EntityType();
        entityType.setId(datos.id);
        entityType.setName(datos.name_item);
        return entityType;
    };

    fromJson(jsonData: any): EntityType[]{
        var entityTypes: EntityType[] = [];

        for(let entityType of jsonData){
            var entityTypeTemp = new EntityType();
            entityTypeTemp.setName(entityType.name_item);
            entityTypes.push(entityTypeTemp);
        }
        return entityTypes;
    }

    getSql(entity: EntityAbstract): string{
        return 'INSERT INTO roles (name_item) VALUES ("' + entity.getName() + '");';
    }

    async getAllItems(): Promise<EntityType[]>{
        return await this.getAll("entity_type",this.getObject);
    }

    async getItem(id: string): Promise<EntityType>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['entity_type',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData)
            validate = validate && entity.hasOwnProperty("name_item");
            
        return validate;
    }
    
    toString(){
        return "Rol Dao";
    }
}