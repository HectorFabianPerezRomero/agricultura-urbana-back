import DaoAbstract from "../generico/abstract/DaoAbstract";
import TypeLog from "../vo/TypeLog";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class TypeLogDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var typeLog = new TypeLog();
        typeLog.setId(datos.id);
        typeLog.setName(datos.name_item);
        return typeLog;
    };

    fromJson(jsonData: any): TypeLog[]{
        var typeLogs: TypeLog[] = [];

        for(let typeLog of jsonData){
            var typeLogTemp = new TypeLog();
            typeLogTemp.setName(typeLog.name_item);
            typeLogs.push(typeLogTemp);
        }
        return typeLogs;
    }

    getSql(entity: EntityAbstract): string{
        return 'INSERT INTO roles (name_item) VALUES ("' + entity.getName() + '");';
    }

    async getAllItems(): Promise<TypeLog[]>{
        return await this.getAll("type_log",this.getObject);
    }

    async getItem(id: string): Promise<TypeLog>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['type_log',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData)
            validate = validate && entity.hasOwnProperty("name_item");
            
        return validate;
    }
    
    toString(){
        return "Type log Dao";
    }
}