import DaoAbstract from "../generico/abstract/DaoAbstract";
import Environment from "../vo/Environment";
import EntityAbstract from "../generico/abstract/EntityAbstract";

export default class EnvironmentDao extends DaoAbstract{

    constructor(){
        super();
    }

    getObject = function(datos: any) {
        var environment = new Environment();
        environment.setId(datos.id);
        environment.setName(datos.name_item);
        environment.$id_config = datos.id_config;
        environment.$id_user = datos.id_user;
        return environment;
    };

    fromJson(jsonData: any): Environment[]{
        var environments: Environment[] = [];

        for(let environment of jsonData){
            var environmentTemp = new Environment();
            environmentTemp.setName(environment.name_item);
            environment.$id_config = environment.id_config;
            environment.$id_user = environment.id_user;
            environments.push(environmentTemp);
        }
        return environments;
    }

    getSql(entity: Environment): string{
        return 'INSERT INTO data (name_item,id_user,id_config) ' +
        'VALUES ("' + entity.getName() + '",' +
        entity.$id_user + ',' +
        entity.$id_config +
        '");';
    }

    async getAllItems(): Promise<Environment[]>{
        return await this.getAll("environment",this.getObject);
    }

    async getItem(id: string): Promise<Environment>{
        return await this.get("SELECT * FROM ?? WHERE id = ?", ['environment',id] ,this.getObject);
    }

    verifyJsonEntity(jsonData: Object[]):boolean{
        var validate = true;
        for(let entity of jsonData){
            validate = validate && entity.hasOwnProperty("name_item") &&
            entity.hasOwnProperty("id_user") &&
            entity.hasOwnProperty("id_config");
        }
            
        return validate;
    }
    
    toString(){
        return "Rol Dao";
    }
}