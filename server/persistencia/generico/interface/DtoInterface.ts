interface DtoInterface {

    /**
     * Código de servicio
     */
    code: number;

    /**
     * Mensaje del servicio
     */
    message: string;

    /**
     * Datos del servicio
     */
    data: any;

    /**
     * Getters y Setters
     */
    getCode(): number;
    getMessage(): string;
    getData(): any;    
    setCode(code: number): void;
    setMessage(message: string): void;
    setData(data: any): void;

    /**
     * Función para setear todos los campos del servicio
     * @param code código del servicio
     * @param message mensaje del servicio
     * @param data datos del servicio
     */
    setFields(code:number , message: string, data: any):any;
}
export default DtoInterface;