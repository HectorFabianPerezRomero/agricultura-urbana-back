import EntityAbstract from "../abstract/EntityAbstract";

interface DaoInterface {
    
    /**
     * Conexión  MySQL
     */
    connection: any;

    /**
     * Entidad para realizar los procesos
     */
    entity?: EntityAbstract[];

    /**
     * Función para realizar la conexión a la base de datos,
     * se ejecuta en la instanciación del DAO
     */
    connectionDB(): void;

    /**
     * Función abstracta para obtener todos los registros de la base de datos
     * convertidos en objetos de cada clase correspondiente
     * 
     * @param table table de la base de datos
     * @param func función custom que retorna el tipo de entidad
     */
    getAll(table: string , func: (datos: any) => any): any;

    /**
     * Función abstracta para obtener un registro en particular de la base de datos
     * @param table tabla de la base de datos
     * @param args argumentos de la consulta: ['tabla',id]
     * @param func función custom que retorna el tipo de entidad
     */
    get(table: string , args: string[], func: (datos: any) => any): any;

    /**
     * Función para convertir la informacion que llega del servicio al
     * objeto
     * @param jsonData datos que llegan por el servicio para parsear al objeto
     */
    parseEntity?(jsonData: string[]) : any[];

    /**
     * Función para guardar las entidades en la base de datos
     * @param entities Arreglo de entidades a guardar en la base de datos
     */
    saveEntities(entities: EntityAbstract[], getSql: (datos: any) => any): any;
}

export default DaoInterface;