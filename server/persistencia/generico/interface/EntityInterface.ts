interface EntityInterface {
    id: number;
    name_item: string;

    getId(): number;
    getName(): string;
    setId(id: number): void;
    setName(name_item: string): void;
    toString(): string;
}   
export default EntityInterface;