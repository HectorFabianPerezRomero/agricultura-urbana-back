import DtoInterface from "../interface/DtoInterface";

export default abstract class DtoAbstract implements DtoInterface{

    code: number;
    message: string;
    data: any;

    constructor(){
        this.code = 0;
        this.message = "message";
        this.data = [];
    }

    getCode(){
        return this.code;
    }
    getMessage(){
        return this.message;
    }
    getData(){
        return this.data;
    }    
    setCode(code: number){
        this.code = code;
    }
    setMessage(message: string){
        this.message = message;
    }
    setData(data: any){
        this.data = data;
    }
    
    setFields(code:number , message: string, data: any){
        this.code = code;
        this.message = message;
        this.data = data;
    }
}