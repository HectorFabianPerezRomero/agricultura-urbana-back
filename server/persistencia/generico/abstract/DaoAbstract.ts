import DaoInterface from "../interface/DaoInterface";
import * as constantes from '../../../negocio/constant/constantes';
import EntityAbstract from "./EntityAbstract";
import * as mysql from 'mysql2/promise';


export default abstract class DaoAbstract implements DaoInterface{
    
    connection: any;

    constructor() {
        this.connectionDB();
    }

    /**
     * Función que verifica el json del servicio para poder convertir la información
     * al objeto
     * @param jsonData datos que llegan por el servicio para parsear al objeto
     * 
     * Se implementa en el DAO de cada clase
     */
    abstract verifyJsonEntity(jsonData: any):boolean;

    /**
     * Función para convertir el json que llega al servicio a un array de objetos
     * @param jsonData Objeto JSON que llega desde el servicio
     */
    abstract fromJson(jsonData: any): EntityAbstract[];

    /**
     * Función para generar SQL para realizar el guardado en la base de datos
     * @param entity Entidad para generar el SQL de guardar
     */
    abstract getSql(entity: EntityAbstract): string;

    /**
     * Función para convertir la información de la base de datos al objeto correspondiente
     * @param datos array de datos de la base de datos
     * 
     * Se implementa en el DAO de cada clase
     */
    abstract getObject(datos: any):any;

    /**
     * Función para retornar todos los registros de la base de datos
     * convertidos en un array de objetos
     * 
     * Se implementa en cada DAO y debe definirse como async
     */
    abstract getAllItems():Promise<EntityAbstract[]>;

    /**
     * Función para obtener un registro en particular de la base de datos
     * y regresar el objeto.
     * 
     * Se implementa en cada DAO y debe definirse como async
     * 
     * @param id id del registro a devolver
     */
    abstract getItem(id: string):Promise<EntityAbstract>;

    async connectionDB(){
        this.connection = await mysql.createConnection({
            host: constantes.db_host,
            user: constantes.db_user_name,
            password: constantes.db_user_pass,
            database: constantes.db_name
        });
    }

    async getAll(table: string, func: (datos: any) => any ): Promise<any> {

        var list: any[] =[];
        let sql = "SELECT * FROM " + table;

        await this.connectionDB();

        const [results] = await this.connection.query(sql);

        for(let result of results){
            list.push(func(result));
        }
        
        return list;
    };

    async get(sql: string, args: string[], func: (datos: any) => any ): Promise<any>{

        await this.connectionDB();

        const [results] = await this.connection.query(sql,args);
        if (results.length > 0) {
            return func(results[0]);
        }
        return false;
    }

    async saveEntities(entities: EntityAbstract[], getSql: (datos: any) => any): Promise<EntityAbstract[]>{
        await this.connectionDB();

        var sql:string = '';
        var index = 0;
        
        for (let entity of entities) {
            sql = getSql(entity);

            let results = await this.connection.query(sql);

            entities[index].setId(results[0].insertId);
            index++;
        }
        return entities;
    }
}