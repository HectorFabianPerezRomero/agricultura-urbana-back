import EntityInterface from '../interface/EntityInterface';

export default abstract class EntityAbstract implements EntityInterface{
    id: number;
    name_item: string;

    constructor(){
        this.id = 0;
        this.name_item = "";
    }

    getId(){
        return this.id;
    }

    getName(){
        return this.name_item;
    }

    setId(id: number): void{
        this.id = id;
    }

    setName(name_item: string): void{
        this.name_item = name_item;
    }

    toString(){
        return this.name_item;
    }
}