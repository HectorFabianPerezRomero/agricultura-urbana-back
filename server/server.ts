import express from 'express';
import bodyParser from 'body-parser';

import HomeController from './negocio/controllers/HomeController';
import masterRoutes from './negocio/routes/MasterRoutes';
import ConfigController from './negocio/controllers/ConfigController';

let homeController = new HomeController();

const server = express();

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.use('/',masterRoutes);

// Configuraciones previas al despliegue del servidor
const configController = new ConfigController();

// configController.checkConfig();

server.listen(3000, () => {
    console.log("Server linstening in port 3000");
});